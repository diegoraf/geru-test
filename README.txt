application
==================

### Requirements

* Python 3.5+
* virtualenvwrapper
* Git


### Installation

```sh
$ mkvirtualenv geru-test

$ workon geru-test

$ git clone https://bitbucket.org/diegoraf/geru-test.git

$ pip install -e .

$ initialize_application_db development.ini

$ pserve development.ini
```

> Serving on http://localhost:6543

### Lib

Name: QuoteIt

Methods:

* get_quotes()            -> Returns all quotes
* get_quote(quote_number) -> Return a particular quote for the given number


Usage:

```python
client = QuoteIt("https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge")
print(client.get_quotes())
```
