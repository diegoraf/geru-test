import requests

# Usage:

# if __name__ == '__main__':
#     client = QuoteIt("https://1c22eh3aj8.execute-api.us-east-1.amazonaws.com/challenge")
#     print(client.get_quotes())


class QuoteIt:

    base_path = None

    def __init__(self, base_path):
        self.base_path = base_path

    def get_quotes(self):
        '''
            Query API and returns dictionary python containing the quotes
            :return: {'quotes': ['Beautiful is better than ugly.', 'Explicit is better than implicit.']}
        '''

        r = requests.get(self.base_path + '/quotes')
        return r.json()

    def get_quote(self, quote_number):
        '''
            Query API and return the corresponding quote
            :return: {'quote': 'Simple is better than complex.'}
        '''

        r = requests.get(self.base_path + '/quotes/' + str(quote_number))
        return r.json()

