from uuid import uuid4

from pyramid.events import NewRequest
from pyramid.events import subscriber

from application.models.access import Access


@subscriber(NewRequest)
def save_access(event):

    session = event.request.session
    if 'identifier' not in session:
        session['identifier'] = str(uuid4())
        session.save()

    dbsession = event.request.dbsession
    dbsession.add(Access(identifier=session['identifier'], page=event.request.path))
