import random

from pyramid.view import view_config
from libs.quoteit.quoteit import QuoteIt


@view_config(route_name='home', renderer='../templates/home.jinja2')
def home(request):
    return {}


@view_config(route_name='quotes', renderer='../templates/quotes.jinja2')
def quotes(request):
    client = QuoteIt(request.registry.settings['quotesclient.url'])
    quotes = client.get_quotes()
    return quotes


@view_config(route_name='quote_detail', renderer='../templates/quote_detail.jinja2')
def quote_detail(request):
    quote_number = request.matchdict['quote_number']

    client = QuoteIt(request.registry.settings['quotesclient.url'])
    response = client.get_quote(quote_number)

    quote = response.get('quote', None)

    return {'quote': quote}


@view_config(route_name='quote_random', renderer='../templates/quote_random.jinja2')
def quote_random(request):
    quote_number = random.randint(1, 18)
    client = QuoteIt(request.registry.settings['quotesclient.url'])
    quote = client.get_quote(quote_number)
    return {'quote': quote['quote'], 'quote_number': quote_number}
