import random

from pyramid.view import view_config
from application.models.access import Access


@view_config(route_name='access', renderer='json')
def access(request):
    session = request.session

    query = request.dbsession.query(Access)
    all = query.filter(Access.identifier == session['identifier']).all()

    return all
