from pyramid.view import view_config
from application.models.access import Access


@view_config(route_name='session', renderer='json')
def session(request):

    query = request.dbsession.query(Access)
    all = query.group_by(Access.identifier).all()

    resp = []
    for i in all:
        resp.append({'identifier': i.identifier})

    return resp
