import datetime

from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    DateTime
)

from .meta import Base


class Access(Base):
    __tablename__ = 'access'

    def __json__(self, request):
        return dict(
            id=self.id,
            identifier=self.identifier,
            date=self.date.isoformat(),
            page=self.page,
        )

    id = Column(Integer, primary_key=True)
    identifier = Column(Text)
    date = Column(DateTime, default=datetime.datetime.utcnow)
    page = Column(Text)


# Index('my_index', Quote.name, unique=True, mysql_length=255)
