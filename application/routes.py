
def integers(*segment_names):
    def predicate(info, request):
        match = info['match']
        for segment_name in segment_names:
            try:
                match[segment_name] = int(match[segment_name])
            except (TypeError, ValueError):
                raise ValueError('invalid url generated')
        return True
    return predicate


param_to_int = integers('quote_number')


def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('quotes', '/quotes')
    config.add_route('quote_detail', '/quotes/{quote_number:[1-9]\d*}')
    config.add_route('quote_random', '/quotes/random')

    config.add_route('access', '/access')
    config.add_route('session', '/session')
